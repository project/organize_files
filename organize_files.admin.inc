<?php

/**
 * @file
 * Admin form for file migrations.
 */

/**
 * Admin form callback.
 */
function organize_files_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  $form['description'] = array(
    '#markup' => t('This will move all files from current location to the
      location which is specified in the content type field settings.'),
  );

  $types = node_type_get_names();
  $form['types'] = array(
    '#type' => 'select',
    '#options' => $types,
    '#multiple' => TRUE,
    '#title' => t('Select Content Type'),
    '#required' => TRUE,
    '#ajax' => array(
      'wrapper' => 'file-fields-container',
      'callback' => 'organize_files_ajax_callback',
    ),
  );

  $form['filefields'] = array(
    '#prefix' => '<div id="file-fields-container">',
    '#suffix' => '</div>',
  );

  // After selecting content type show file and image fields.
  if (!empty($form_state['values']['types'])) {
    $file_fields = array();
    $selected_node_types = $form_state['values']['types'];
    foreach ($selected_node_types as $type) {
      $fields = field_info_instances('node', $type);

      $form['filefields'][$type . '_container'] = array(
        '#type' => 'fieldset',
        '#title' => t('Select Fields for @type', array('@type' => $type)),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      foreach ($fields as $field_name => $field_details) {
        if (in_array($field_details['widget']['module'], array('image', 'file'))) {
          $form['filefields'][$type . '_container'][$field_name] = array(
            '#type' => 'fieldset',
            '#title' => t('Settings for @field', array('@field' => $field_details['label'])),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          );
          $form['filefields'][$type . '_container'][$field_name]['enable'] = array(
            '#type' => 'checkbox',
            '#option' => $field_name,
            '#title' => t('Migrate Files'),
          );
          $form['filefields'][$type . '_container'][$field_name]['filepath'] = array(
            '#type' => 'textfield',
            '#title' => t('File migration Path for this field'),
            '#description' => t('If you leave this field blank, it will take
              path from the content type field setting directory, you can also
              use replacement patterns'),
          );
          $form['filefields'][$type . '_container'][$field_name]['token_help'] = array(
            '#type' => 'fieldset',
            '#title' => t('Replacement patterns'),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
            '#description' => theme('token_tree', array('token_types' => array('file'), 'global_types' => FALSE)),
            '#weight' => 10,
          );
        }
      }
    }
  }

  $form['redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Redirect'),
    '#disabled' => module_exists('redirect') ? FALSE : TRUE,
    '#description' => t('To enable this you will have to enable redirect module'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Move Files'),
  );
  return $form;
}

/**
 * Ajax Callback.
 *
 * @see organize_files_form()
 */
function organize_files_ajax_callback($form, $form_state) {
  return $form['filefields'];
}

/**
 * Submit Callback.
 *
 * @see organize_files_form()
 */
function organize_files_form_submit($form, &$form_state) {
  $types = $form_state['values']['types'];
  $filefields = isset($form_state['values']['filefields']) ? $form_state['values']['filefields'] : NULL;
  $selected_fields = organize_files_process_input($types, $filefields);
  $redirect = is_null($form_state['values']['redirect']) ? FALSE : TRUE;

  // Set a batch request.
  $batch = array(
    'operations' => array(
      array('organize_files_batch_callback',
        array(
          $selected_fields,
          $redirect,
        ),
      ),
    ),
    'finished' => 'organize_files_batch_finished',
    'title' => t('Process file moving'),
    'init_message' => t('File moving process started'),
    'error_message' => t('Process has encountered an error'),
    'file' => drupal_get_path('module', 'organize_files') . '/organize_files.batch.inc',
  );
  batch_set($batch);
}

/**
 * Process the types array according to code.
 *
 * @param array $types
 *   Array containing information about content type and fields.
 * @param array $filefields
 *   Information about file fields.
 *
 * @return array
 *   Processed array.
 */
function organize_files_process_input($types, $filefields) {
  $selected_fields = array();
  // Create an array of content type and related fields.
  foreach ($types as $type) {
    $content_type_fields = $filefields[$type . '_container'];
    if (isset($content_type_fields)) {
      foreach ($content_type_fields as $field_name => $value) {
        if ($value['enable'] === 1) {
          if (isset($selected_fields[$type]) && is_array($selected_fields[$type])) {
            $selected_fields[$type][$field_name] = $value['filepath'];
            continue;
          }
          $selected_fields[$type] = array();
          $selected_fields[$type][$field_name] = $value['filepath'];
        }
      }
    }
  }
  return $selected_fields;
}
