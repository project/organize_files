<?php

/**
 * @file
 * Batch functions.
 */

/**
 * Batch Operation Callback.
 */
function organize_files_batch_callback($fields, $redirect, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['results']['details'] = $fields;
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_file'] = 0;
    $context['sandbox']['max'] = organize_files_file_count($fields);
    // This variable holds the pointer value of fields and content type array.
    $context['sandbox']['slice_position'] = 0;
  }
  $limit = 50;
  // Here we process one content type and selected fields.
  if ($context['sandbox']['slice_position'] <= count($fields)) {
    // Process one content type at a time.
    $selected_fields = array_slice($fields, $context['sandbox']['slice_position'], 1);
    $files = organize_files_files_batch($selected_fields, $limit, $context['sandbox']['progress']);
    // Provide the destination for file fields of different content types.
    $destinations = organize_files_get_destination($selected_fields);
    if (!empty($files)) {
      foreach ($files as $fid => $field_name) {
        $file = file_load($fid);
        $destination = file_build_uri($destinations[$field_name]);
        // Create the file directory if it does not exists.
        if (is_object($file) && !is_null($destination)) {
          $destination = token_replace($destination, array('file' => $file));
          file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
          // Move file to destination, if file already exists do nothing.
          $success = file_move($file, $destination, FILE_EXISTS_ERROR);
          if ($redirect && $success) {
            // Create redirect from source to destination.
            $target = file_uri_target($file->uri);
            $source = 'sites/default/files/' . $target;
            $destination = file_create_url($success->uri);
            if (strlen($source) <= 255 && strlen($destination) <= 255) {
              $args = array(
                'source' => $source,
                'source_options' => array(),
                'redirect' => $destination,
                'redirect_options' => array(),
                'language' => LANGUAGE_NONE,
              );
              $redirect = new stdClass();
              module_invoke('redirect', 'object_prepare', $redirect, $args);
              module_invoke('redirect', 'save', $redirect);
              // Store some result for post-processing in the finished callback.
              $context['results']['success'][] = t('@filename successfully moved', array('@filename' => $success->uri));
            }
            else {
              $context['results']['failed'][] = array(
                'source' => $source,
                'redirect' => $destination,
              );
            }
          }
          $context['results']['total'][] = $fid;
        }

        // Update our progress information.
        $context['sandbox']['progress']++;
        $context['sandbox']['current_file'] = $fid;
        $context['message'] = t('Moving file %filename', array('%filename' => $file->uri));
      }
    }
    else {
      // If we are finished with one content type files, move to next.
      $context['sandbox']['slice_position']++;
    }
  }
  else {
    $context['sandbox']['progress'] = $context['sandbox']['max'] + 1;
  }
  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch 'finished' callback.
 */
function organize_files_batch_finished($success, $results, $operations) {
  if ($success) {
    $results['success'] = !empty($results['success']) ? $results['success'] : array();
    // Here we do something meaningful with the results.
    $total = isset($results['total']) ? count($results['total']) : 0;
    $success_message = t('@count items successfully moved out of @total:', array(
      '@count' => count($results['success']),
      '@total' => $total,
    )
    );
    $message = $success_message;
    $message .= theme('item_list', array('items' => $results['success']));
    drupal_set_message($message);
    organize_files_update_logs($results, $message, $success_message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments',
      array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      )
    );
    drupal_set_message($message, 'error');
  }
}

/**
 * Get destination for selected fields of a content type.
 *
 * @param array $selected_fields
 *   Format: array('type' => array('field1', 'field2')).
 *
 * @return array
 *   Fromat: array('field1' => 'value'), return destination value.
 */
function organize_files_get_destination($selected_fields) {
  $destinations = array();
  foreach ($selected_fields as $type => $fields) {
    foreach ($fields as $field => $value) {
      if (!empty($value)) {
        $destinations[$field] = $value;
      }
      else {
        $field_settings = field_info_instance('node', $field, $type);
        // Get file directory configured in the content type field settings.
        $destinations[$field] = isset($field_settings['settings']['file_directory']) ? token_replace($field_settings['settings']['file_directory']) : NULL;
      }
    }
  }
  return $destinations;
}

/**
 * Get the batch of files to process in batch process.
 *
 * @param array $selected_fields
 *   Format: array('type' => array('field1', 'field2')).
 * @param int $limit
 *   Limit for array.
 * @param int $start
 *   Starting position of array.
 *
 * @return array
 *   Give sliced array from start to the limit.
 */
function organize_files_files_batch($selected_fields, $limit, $start) {
  $result = array();
  foreach ($selected_fields as $type => $fields) {
    $temp_fields = array_keys($fields);
    foreach ($temp_fields as $field_name) {
      // Query for extracting the fids from file tables.
      $table = 'field_data_' . $field_name;
      $query = db_select($table, 'f')
        ->fields('f', array($field_name . '_fid'))
        ->condition('bundle', $type)
        ->execute()
        ->fetchAllKeyed(0, 0);

      // This is done to relate the fid with the fieldname, so that I can
      // get the directory of this field in batch process.
      foreach ($query as $fid) {
        $result[$fid] = $field_name;
      }
    }
  }

  // Slicing the array keeping the keys same.
  $batch = array_slice($result, $start, $limit, TRUE);
  return $batch;
}

/**
 * Count total results for one batch.
 *
 * @param array $fields
 *   Array containing all the content type and fields.
 *
 * @return int
 *   Count of the fids in these fields.
 */
function organize_files_file_count($fields) {
  $count = 0;
  foreach ($fields as $type => $fields) {
    $key_fields = array_keys($fields);
    foreach ($key_fields as $field_name) {
      $query = NULL;
      $query = db_select('field_data_' . $field_name, 'ft')
        ->condition('entity_type', 'node')
        ->condition('bundle', $type)
        ->fields('ft', array($field_name . '_fid'));
      $result = $query->execute()->rowCount();
      $count += $result;
    }
  }
  return $count;
}

/**
 * Update logs as per the migration.
 *
 * @param array $result
 *   Array of fields. for e.g., array('type' => array('field1', 'field2')).
 * @param string $message
 *   Details about each file in the migration.
 * @param string $success_message
 *   How many files out of total were successfully migrated.
 */
function organize_files_update_logs($result, $message, $success_message) {
  global $user;
  $details = $result['details'];
  foreach ($details as $type => $fields) {
    $data = array(
      'fields' => $fields,
      'message' => $message,
    );
    $data = serialize($message);
    $logs = array(
      'uid' => $user->uid,
      'timestamp' => REQUEST_TIME,
      'type' => $type,
      'data' => $data,
      'message' => $success_message,
    );
    drupal_write_record('organize_files_logs', $logs);
    if (isset($result['failed']) && !empty($result['failed'])) {
      organize_files_update_failed_redirects($logs['lid'], $result['failed']);
    }
  }
}

/**
 * Update the logs for failed redirects.
 *
 * @param int $lid
 *   LID of logs.
 * @param array $failed_redirects
 *   Array of failed redirects. (array('source' => '', 'redirect' => '')).
 */
function organize_files_update_failed_redirects($lid, $failed_redirects) {
  foreach ($failed_redirects as $failed_redirect) {
    $logs = array(
      'lid' => $lid,
      'source' => $failed_redirect['source'],
      'redirect' => $failed_redirect['redirect'],
    );
    drupal_write_record('organize_files_failed_redirects', $logs);
  }
}
